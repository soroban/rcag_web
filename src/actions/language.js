import store from '../store/store';

function selectLang(lang) {
    if (lang === 'en') {
        store.dispatch({type: 'LANGUAGE_EN'})
    }
    else if (lang === 'ja') {
        store.dispatch({type: 'LANGUAGE_JA'})
    }else{
        store.dispatch({type: 'LANGUAGE_EN'})
    }
}



module.exports = {
    selectLang,
}