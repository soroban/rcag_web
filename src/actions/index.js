import reviewsActions from './loadReviews'
import languageActions from './language'

module.exports = {
    ...reviewsActions,
    ...languageActions,
}
