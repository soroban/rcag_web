import {firebaseDb} from '../firebase/'

function loadReviews(lang) {
    const ref = firebaseDb.ref('reviews/' + lang);
    return dispatch => {
            ref.off()
            ref.on('value',
            (snapshot) => {dispatch(loadReviewsSuccess(snapshot))},
            (error) => {dispatch(loadReviewsError(error))}
        )
    }
}

function loadReviewsSuccess(snapshot){
    return {
        type: 'REVIEWS_RECEIVE_DATA',
        data: snapshot.val()
    }
}

function loadReviewsError(error){
    return {
        type: 'REVIEWS_RECIVE_ERROR',
        data: error.message
    }
}

module.exports = {
    loadReviews,
}
