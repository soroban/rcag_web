import { createStore, applyMiddleware } from 'redux';
import { combineReducers } from 'redux';
import thunk from 'redux-thunk';
import reviews from '../reducers/reviews';
import language from '../reducers/language';
import { localeReducer } from 'react-localize-redux';


const store = createStore(combineReducers({reviews, language, locale: localeReducer}),
    {},
    applyMiddleware(thunk)
);

export default store;