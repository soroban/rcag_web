function language(state = [], action){
    if(action.type === "LANGUAGE_EN"){
        return 'en'
    }
    if(action.type === "LANGUAGE_JA"){
        return 'ja'
    }
    if (typeof state !== 'undefined') {
        return state
    }
}

export default language;