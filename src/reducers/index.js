import { combineReducers } from 'redux';
import reviews from './reviews';
import language from './language';

const  reducer = combineReducers({
    reviews,language
});

export default reducer;