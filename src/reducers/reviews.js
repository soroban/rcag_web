function reviews(state = [], action){
//const review = ( state = initailAppState, action) => {
    if(action.type === "REVIEWS_RECEIVE_DATA"){
        let reviews = []
        if (action.data){
            Object.keys(action.data).forEach(key =>{
                let rec = action.data[key];
                reviews.push({
                    key: key,
                    content: rec.content,
                    name: rec.name,
                    rating: rec.rating,
                    title: rec.title
                })
            });
        }
        return {
            ...state,
            reviews: reviews
        }
    }
    else {
        return state;
    }
}

export default reviews;