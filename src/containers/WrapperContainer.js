import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import store from '../store/store';
import { loadReviews } from '../actions';
import Wrapper from '../components/Wrapper'
import myTranslate from '../tools/myTranslate'
import * as actions from '../actions';
import createHistory from 'history/createBrowserHistory';
export const history = createHistory()

class WrapperContainer extends Component {
    componentDidMount () {
        const lang = this.props.language;
        store.dispatch(loadReviews(lang));
    }
    componentWillUpdate() {
        const lang = this.props.language
        var t = new myTranslate(lang);
        document.title = t.taranslate("h1");
        document.documentElement.lang = lang
    }
    render(){
        const {reviews}=this.props.reviews;
        const lang=this.props.language;
        // // store.dispatch(loadReviews(lang));

        return(
            <Wrapper
                reviews={reviews}
                lang={lang}
            />
        );
    }
}

// var MusicListViewModel = React.createClass( {
//     render: function() {
//         console.log(this.props)
//         return MusicListView( this, this.props.langugage, this.props.current );
//     }
// });


const mapStateToProps=(state) => {
    return {
        reviews: state.reviews,
        language: state.language

    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WrapperContainer);