import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import WrapperContainer from './containers/WrapperContainer';
import store from './store/store';
import createHistory from 'history/createBrowserHistory';
export const history = createHistory()



function handleNavigation(location, action) {
    // e.g., 'examples/123/' => ["examples", "123"]
    const pathList = location.pathname.split('/').filter(o => o !== '')
    const lang = pathList[0]

    //store.dispatch(loadReviews(lang));
    if (lang === 'en') {
        store.dispatch({type: 'LANGUAGE_EN'})
        history.push("/" + lang);
    }
    else if (lang === 'ja') {
        store.dispatch({type: 'LANGUAGE_JA'})
        history.push("/" + lang);
    }else{
        history.push("/");
        store.dispatch({type: 'LANGUAGE_EN'})
    }


}

handleNavigation(history.location)
history.listen(handleNavigation)

// render(
//     <Hellow name="world!" lang="aaaa"/>,
//     document.getElementById('test')
// );

render(
    <Provider store={store}>
        <WrapperContainer />
    </Provider>,
    document.getElementById("root")
)