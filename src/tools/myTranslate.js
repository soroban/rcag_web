import React from 'react';

export default class myTranslate{
    constructor(lang) {
        this.lang = lang
        this.txt = require('../asset/global.locale.json');
    }

    taranslate(target){
        var br = React.createElement('br');
        var regex = /(<br \/>|<br>)/g;
        return this.txt[target][this.lang].split(regex).map(function(line, index) {
            return line.match(regex) ? <br key={"key_" + index} /> : line;
        });
    }
};