var React    = require( 'react' );
import { render } from 'react-dom';

const json = require('./asset/global.locale.json');
//const base = json.en

export default class Header extends React.Component {
    render() {

        return(
            <header id="header" className="alt">
                <span className="logo"><img className="top-logo" src="images/logo.png" width="85px" /></span>
                <h1 id="h1">目標管理,目標達成と習慣化のためのアプリ</h1>
                <p>- 自分へのご褒美を上手に使って目標達成 -</p>
                <a className="it-download" href="https://itunes.apple.com/jp/app/%E8%87%AA%E5%88%86%E3%83%AB%E3%83%BC%E3%83%AB%E3%83%9D%E3%82%A4%E3%83%B3%E3%83%88%E3%83%97%E3%83%AD%E3%82%B0%E3%83%A9%E3%83%A0-%E7%9B%AE%E6%A8%99%E7%AE%A1%E7%90%86-%E7%9B%AE%E6%A8%99%E9%81%94%E6%88%90%E3%81%AE%E7%BF%92%E6%85%A3%E5%8C%96%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AE%E8%A8%98%E9%8C%B2%E3%82%A2%E3%83%97%E3%83%AA/id1000356937?mt=8" style="display:inline-block;overflow:hidden;" target="_blank" rel="noopener noreferrer">
                    <img src={'images/itunes_app.png'} alt="app download" width="165px" className="download-itunes" />
                </a>
            </header>
        );
    }
}

