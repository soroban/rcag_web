import React, {Component} from 'react'
import ReviewsList from '../components/ReviewsList'
import LangSel from '../components/LangSel'
import myTranslate from '../tools/myTranslate'

class Main extends Component {
    render(){
        const {reviews}=this.props;
        const {lang} = this.props;

        var itdownload = {
            display:"inline-block",
            overflow:"hidden"
        };

        var t = new myTranslate(lang);
        var it_rg_url = "https://itunes.apple.com/us/app/my-rules-rewards-program-mrrp-achieve-ones-goal-build/id1000356937?mt=8"
        var it_mt_url = "https://itunes.apple.com/us/app/meetrow-app-gathers-lineup-for-music-festivals/id1099070037?mt=8"
        if(lang === "ja") {
            it_rg_url = "https://itunes.apple.com/jp/app/%E8%87%AA%E5%88%86%E3%83%AB%E3%83%BC%E3%83%AB%E3%83%9D%E3%82%A4%E3%83%B3%E3%83%88%E3%83%97%E3%83%AD%E3%82%B0%E3%83%A9%E3%83%A0-%E7%9B%AE%E6%A8%99%E7%AE%A1%E7%90%86-%E7%9B%AE%E6%A8%99%E9%81%94%E6%88%90%E3%81%AE%E7%BF%92%E6%85%A3%E5%8C%96%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AE%E8%A8%98%E9%8C%B2%E3%82%A2%E3%83%97%E3%83%AA/id1000356937?mt=8"
            it_mt_url = "https://itunes.apple.com/jp/app/metoro-u-fesunomatome-rokkuinjapan/id1099070037?mt=8"
        }
        var dl_image_path = "images/"+lang+"_itunes_app.png";

        return(
            <div id="wrapper">
                <header id="header" className="alt">
                    <span className="logo"><img className="top-logo" src={'images/logo.png'}  alt="logo"  width="85px" /></span>
                    <h1 id="h1">{ t.taranslate("h1") }</h1>
                    <p>- { t.taranslate("#header>p") } -</p>
                    <a className="it-download" href={it_rg_url} style={itdownload} target="_blank" rel="noopener noreferrer">
                        <img src={dl_image_path} alt="app download" width="165px" className="download-itunes" />
                    </a>
                </header>
                <nav id="nav" className="alt">
                    <ul>
                        <li><a href="#intro" className="active">{ t.taranslate("#nav>ul>1") }</a></li>
                        <li><a href="#first">{ t.taranslate("#nav>ul>2") }</a></li>
                        <li><a href="#second">{ t.taranslate("#nav>ul>3") }</a></li>
                        <li><a href="#cta">{ t.taranslate("#nav>ul>4") }</a></li>
                    </ul>
                </nav>

                <div id="main">
                    <div id="test"></div>
                    {/* Introduction */}
                    <section id="intro" className="main">
                        <div className="spotlight">
                            <div className="content">
                                <header className="major">
                                    <h2>{ t.taranslate("#nav>ul>1") }</h2>
                                </header>
                                <p>{ t.taranslate("#intro>div>div>p") }</p>
                            </div>
                            <span className="image"><img src="images/pic01.jpg" alt="" /></span>
                        </div>
                    </section>

                    {/* First Section */}
                    <section id="first" className="main special">
                        <header className="major">
                            <h2>{ t.taranslate("#nav>ul>2") }</h2>
                        </header>
                        <ul className="features">
                            <li>
                                <span className="icon major style1 fa-code"></span>
                                <h3>{ t.taranslate("#first>ul>li:1>h3") }</h3>
                                <p className="left-pg">
                                    { t.taranslate("#first>ul>li:1>p") }
                                </p>
                            </li>
                            <li>
                                <span className="icon major style3 fa-copy"></span>
                                <h3>{ t.taranslate("#first>ul>li:2>h3") }</h3>
                                <p className="left-pg">
                                    { t.taranslate("#first>ul>li:2>p") }
                                </p>
                            </li>
                            <li>
                                <span className="icon major style5 fa-diamond"></span>
                                <h3>{ t.taranslate("#first>ul>li:3>h3") }</h3>
                                <p className="left-pg">
                                    { t.taranslate("#first>ul>li:3>p") }
                                </p>
                            </li>
                        </ul>
                        <footer className="major">
                            { t.taranslate("#first>footer.tetx") }
                            <ul className="actions">
                                <li><a href={ t.taranslate("#first>footer>ul>li:1>a.link") } target="_blank" rel="noopener noreferrer">{ t.taranslate("#first>footer>ul>li:1>a.tetx") }</a></li>
                                <li><a href={ t.taranslate("#first>footer>ul>li:2>a.link") } target="_blank" rel="noopener noreferrer">{ t.taranslate("#first>footer>ul>li:2>a.tetx") }</a></li>
                            </ul>
                            <a className="it-download" href={it_rg_url} style={itdownload} target="_blank" rel="noopener noreferrer">
                                <img src={dl_image_path} alt="app download" width="165px" className="download-itunes" />
                            </a>
                        </footer>
                    </section>

                    {/* Second Section */}
                    <section id="second" className="main special">
                        <header className="major">
                            <h2>{ t.taranslate("#nav>ul>3") }</h2>
                            <p>{ t.taranslate("#second>header>p") }</p>
                        </header>
                        <ul className="statistics">
                            <li className="style1">
                                <span className="icon fa-edit"></span>
                                <strong></strong> { t.taranslate("#second>ul>li.style1") }
                            </li>
                            <li className="style2">
                                <span className="icon fa-list"></span>
                                <strong></strong>{ t.taranslate("#second>ul>li.style2") }
                            </li>
                            <li className="style3">
                                <span className="icon fa-calendar"></span>
                                <strong></strong>{ t.taranslate("#second>ul>li.style3") }
                            </li>
                            <li className="style4">
                                <span className="icon fa-child"></span>
                                <strong></strong> { t.taranslate("#second>ul>li.style4") }
                            </li>
                            <li className="style5">
                                <span className="icon fa-line-chart "></span>
                                <strong></strong> { t.taranslate("#second>ul>li.style5") }
                            </li>
                        </ul>
                        <h3 className="left-pg">- { t.taranslate("#second>ul>li.style1") } - </h3>
                        <p className="content">{ t.taranslate("#second>p:1") }</p>
                        <h3 className="left-pg">- { t.taranslate("#second>ul>li.style2") } - </h3>
                        <p className="content">{ t.taranslate("#second>p:2") }</p>
                        <h3 className="left-pg">- { t.taranslate("#second>ul>li.style3") } - </h3>
                        <p className="content">{ t.taranslate("#second>p:3") }</p>
                        <h3 className="left-pg">- { t.taranslate("#second>ul>li.style4") } -</h3>
                        <p className="content">{ t.taranslate("#second>p:4") }</p>
                        <h3 className="left-pg">- { t.taranslate("#second>ul>li.style5") } -</h3>
                        <p className="content">{ t.taranslate("#second>p:5") }</p>
                        <footer className="major">
                        </footer>
                    </section>
                    {/* Get Started  */}
                    <section id="cta" className="main special">
                        <header className="major">
                            <h2>{ t.taranslate("#nav>ul>4") }</h2>
                            <p>{ t.taranslate("#cta>header>p") }</p>
                        </header>
                        <footer className="major">
                            <ul className="actions">
                                <a className="it-download" href={it_rg_url} style={itdownload} target="_blank" rel="noopener noreferrer">
                                    <img src={dl_image_path} alt="app download" width="165px" className="download-itunes" />
                                </a>
                            </ul>
                        </footer>
                        <ReviewsList
                            reviews={reviews}
                            lang={lang}
                        />
                        <a className="it-download" href={it_rg_url} style={itdownload} target="_blank" rel="noopener noreferrer">
                            <img src={dl_image_path} alt="app download" width="165px" className="download-itunes" />
                        </a>
                    </section>
                </div>
                <footer id="footer">
                    <section>
                        <h2>{ t.taranslate("#footer>section:1>h2") }</h2>
                        <p><a href="https://meetrow.com" target="_blank" rel="noopener noreferrer" >{ t.taranslate("#footer>section:1>p>a") }</a></p>
                        <a className="it-download" href={it_mt_url} style={itdownload} target="_blank" rel="noopener noreferrer">
                            <img src={dl_image_path} alt="app download" width="165px" className="download-itunes" />
                        </a>
                        {/*<!--<ul className="actions">-->*/}
                        {/*<!--<li><a href="generic.html" className="button">メートロゥ</a></li>-->*/}
                        {/*<!--</ul>-->*/}
                    </section>
                    <section>
                        <dl className="alt">
                            <h2></h2>
                            <dt><br /></dt>
                            <dd><br /></dd>
                            {/*<!--<dt>Email</dt>-->*/}
                            {/*<!--<dd><a href="#">information@untitled.tld</a></dd>-->*/}
                            <LangSel
                                lang={lang}
                            />
                        </dl>
                        <ul className="icons">
                            <li><a href="https://twitter.com/soroban11" target="_blank" rel="noopener noreferrer" className="icon fa-twitter alt"><span className="label">Twitter</span></a></li>
                            {/*<!--<li><a href="#" className="icon fa-facebook alt"><span className="label">Facebook</span></a></li>-->*/}
                            {/*<!--<li><a href="#" className="icon fa-instagram alt"><span className="label">Instagram</span></a></li>-->*/}
                            {/*<!--<li><a href="#" className="icon fa-github alt"><span className="label">GitHub</span></a></li>-->*/}
                            {/*<!--<li><a href="#" className="icon fa-dribbble alt"><span className="label">Dribbble</span></a></li>-->*/}
                        </ul>
                    </section>
                    <p className="copyright">&copy; soroban.com. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
                </footer>
            </div>
        )
    }
}
export default Main