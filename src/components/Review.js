import React, {Component} from 'react'
import myTranslate from '../tools/myTranslate'

class Review extends Component {
    render(){
        const {lang} = this.props;
        var t = new myTranslate(lang);
        const {title,content,name,rating} = this.props;
        // const {review} = this.props;
        // //console.log(this.props.reviews);
        var strRating = "";
        for(var i=0; i < rating;i++){
            strRating += '★'
        }

        return(
            <div className="review">
                <div className="review-top"><span className="review-title">{title}</span>　{strRating}</div>
                <div className="review-name">{ t.taranslate("user") } {name}</div>
            <div className="review-content"> {content}</div>
            </div>
        )
    }
}
export default Review;