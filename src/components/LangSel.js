import React, {Component} from 'react'
//import myTranslate from '../tools/myTranslate'
import * as actions from '../actions';
import store from '../store/store';
import { loadReviews } from '../actions';
import createHistory from 'history/createBrowserHistory';
export const history = createHistory()


class LangSel extends Component {
    render(){
        const {lang} = this.props;
//        var t = new myTranslate(lang);

        return(
            <select defaultValue={lang} onChange={this.selectLang}>
                <option value="en">English</option>
                <option value="ja">日本語</option>
            </select>
        )
    }

    selectLang(event){
        var lang = event.target.value
        store.dispatch(loadReviews(lang));
        history.push("/" + lang);
        actions.selectLang(lang);
    }
}

export default LangSel;