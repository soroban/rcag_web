import React, {Component} from 'react'
import Review from './Review';
import myTranslate from '../tools/myTranslate'

class ReviewsList extends Component {

    render(){
        const {lang} = this.props;
        var t = new myTranslate(lang);

        const {reviews} = this.props;

        let list = []
        for(var i in reviews){
            list.push(
                <Review key={i} title={reviews[i].title}
                        name={reviews[i].name}
                        content={reviews[i].content}
                        rating={reviews[i].rating}
                        lang={lang}
                />
            );
        }
        return(
            <div className="review-list"><div className="review-head">{ t.taranslate("review") }</div>{list}</div>
        )
    }
}

export default ReviewsList;